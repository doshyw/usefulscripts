# Facebook Data Parser

A simple script that is able to parse the JSON data dump that can be requested from Facebook at [https://www.facebook.com/dyi](https://www.facebook.com/dyi).

This script will output:
- A single `output_message.json` file that contains a set of information about the particular chat you are in, and a record of all the messages (prior to this, the dump contains a set of disparate message files that represent this)
- A folder structure that contains the media sent in the chat separated into audio, photos, and videos, and tags them uniquely with their send date, send time, sender name, and a section of the original unique ID (prior to this, the dump has the filenames as essentially random numeric strings, making them a pain to make sense of).

To-do items:
- Enhance functionality for pathing (eliminate need to place script relative to parsing directory).
- Add chat search/display functionality.
