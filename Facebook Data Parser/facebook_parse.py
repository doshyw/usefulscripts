from datetime import datetime, timedelta, timezone
import json
import os
import re
from shutil import rmtree, copy2
from typing import Dict

# participants: List[{name: str}]
# parsed_participants: List[{
#   name: str
#   nickname: List[str]
# }]
# messages: List[{
#   sender_name: str
#   timestamp_ms: int
#   content: str
#   type: Enum[str] ('Generic', 'Share')
#   is_unsent: bool
#   audio_files: List[{
#     uri: str
#     creation_timestamp: int
#   }]
#   photos: List[{
#     uri: str
#     creation_timestamp: int
#   }]
#   videos: List[{
#     uri: str
#     creation_timestamp: int
#     thumbnail: {
#       uri: str
#     }
#   }]
#   reactions: Optional[List[{
#     reaction: str
#     actor: str
#   }]]
#   share: Optional[{link: str}]
# }]
# title: str
# thread_path: str
# image: {
#   uri: str
#   creation_timestamp: int
# }

global_json = {
    'participants': [],
    'parsed_participants': [],
    'messages': [],
    'title': '',
    'thread_path': '',
    'image': {},
    'msg_count': 0
}

valid_strings = []

# Gets the total number of Facebook message_?.json files
def count_msg_files() -> int:
    # Get a list of strings for each file/folder in the cwd
    dir_list = os.listdir()
    
    # [HARDCODED] Create a regex object for matching the file format
    regx_files = re.compile('message_\d+.json')
    
    # Iterate over all directory elements
    msg_count = 0
    for dir_elem in dir_list:
        # If the element matches the format, and the contained number
        #   is greater than previously seen, store it
        if(regx_files.fullmatch(dir_elem)):
            # [HARDCODED] Extract substring for file number
            num_str = dir_elem[8:-5]
            num = int(num_str)
            if(num > msg_count):
                msg_count = num         
    return msg_count

# Reads the JSON struct from the message file with number 'file_num'
def get_json_dict(file_num: int) -> Dict:
    # [HARDCODED] Opens the message file with the given number
    f = open(f'message_{file_num}.json')
    json_data = json.load(f)
    f.close()
    return json_data

# Performs an initial stash to the global JSON dict based on a top file
def stash_initial(file_num: int) -> None:
    global global_json
    json_data = get_json_dict(file_num)
    global_json['participants'] = json_data['participants']
    global_json['title'] = json_data['title']
    global_json['thread_path'] = json_data['thread_path']
    if('image' in json_data.keys()):
        global_json['image'] = json_data['image']

# Updates the global JSON dict based on subsequent files
def stash_loop(file_num: int) -> None:
    global global_json
    json_data = get_json_dict(file_num)
    global_json['messages'].extend(json_data['messages'])

def stash_final() -> None:
    global global_json
    global_json['messages'].sort(key=lambda x: x['timestamp_ms'])
    global_json['msg_count'] = len(global_json['messages'])
    f = open('output_message.json', 'w')
    json.dump(global_json, f, indent=1, separators=(', ', ': '))
    f.close()

def get_message_keys() -> None:
    global global_json
    for dictionary in global_json['messages']:
        for k in dictionary.keys():
            if k not in valid_strings:
                valid_strings.append(k)
    for str in valid_strings:
        print(str)

def get_ts_string(timestamp: int) -> str:
    return datetime.fromtimestamp(timestamp, tz=timezone(timedelta(hours=10))).strftime('%Y_%m_%d_%H%M')

def rename_images() -> None:
    global global_json
    rmtree('output', ignore_errors=True)
    os.mkdir('output')
    os.mkdir('output/audio')
    os.mkdir('output/photos')
    os.mkdir('output/videos')
    user_folder = os.getcwd().split('/')[-1].split('\\')[-1]
    for dictionary in global_json['messages']:
        dict_keys = dictionary.keys()
        if('audio_files' in dict_keys):
            dir_location = 'audio_files'
        elif('photos' in dict_keys):
            dir_location = 'photos'
        elif('videos' in dict_keys):
            dir_location = 'videos'
        else:
            continue
        
        for item_dict in dictionary[dir_location]:
            file_location = item_dict['uri'].split('/')
            if(len(file_location) < 3 or file_location[2] != user_folder):
                continue
            directory = file_location[-2]
            old_filename = file_location[-1]
            file_extension = old_filename.split('.')[-1]
            uid = old_filename.split('.')[-2].split('_')[-1]
            
            new_filename = get_ts_string(item_dict['creation_timestamp'])
            new_filename += '_' + dictionary['sender_name'].split(' ')[0] + '_' + uid + '.' + file_extension
            
            try:
                copy2(f'{directory}/{old_filename}', f'output/{directory}/{new_filename}')
            except Exception:
                pass

def main():
    # Get the total number of message files
    file_count = count_msg_files()
    
    for file_num in range(1, file_count + 1):
        if(file_num == 1):
            stash_initial(file_num)
        stash_loop(file_num)
    
    stash_final()
    rename_images()

if __name__ == '__main__':
    main()