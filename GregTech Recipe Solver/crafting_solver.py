import math
from typing import Dict, List
from recipes import Item, RecipeBook, get_full_book

def summarise(item_dict: Dict[Item, int], recipes: RecipeBook) -> List[Dict[Item, int]]:
    items = list(item_dict.keys())
    nums = list(item_dict.values())
    item_tier = max([x.value for x in items]) // 100
    response: List[Dict[Item, int]]
    response = [{} for i in range(item_tier + 1)]
    for i in range(len(items)):
        response[item_tier][items[i]] = nums[i]
    # Procedurally break down item tiers
    for i in range(item_tier - 1, -1, -1):
        to_pop = []
        # For each item in previous tier
        for itm_craft, amt_craft in response[i + 1].items():
            # If this item should be broken down in this step
            if itm_craft.value // 100 == i + 1:
                # Get its recipe and move each component to the next tier
                r = recipes.get_recipe(itm_craft)
                for itm_recipe, amt_recipe in r.components.items():
                    if itm_recipe not in response[i]:
                        response[i][itm_recipe] = 0
                    response[i][itm_recipe] += amt_recipe * math.ceil(amt_craft / r.num_produced)
            # Otherwise
            else:
                # Move the item to the next tier
                if itm_craft not in response[i]:
                    response[i][itm_craft] = 0
                response[i][itm_craft] += amt_craft
                to_pop.append(itm_craft)
        
        for item in to_pop:
            response[i + 1].pop(item)
    
    return response

def print_summary(summary: List[Dict[Item, int]]):
    for i in range(len(summary)):
        print(f"============")
        print(f"Step {i + 1}:")
        print(f"============")
        temp = list(summary[i].keys())
        temp.sort(key=lambda x: x.name)
        for k in temp:
            num = summary[i][k]
            if(k.name.startswith("LIQUID")):
                if(num // 1000 > 0):
                    print(f"{k.name}: {num // 1000}B + {num % 1000}mB")
                else:
                    print(f"{k.name}: {num}mB")
            else:
                if(num // 64 > 0):
                    print(f"{k.name}: {num // 64}s + {num % 64}")
                else:
                    print(f"{k.name}: {num}")
        print("")

def main():
    book = get_full_book()
    item_dict = {
        Item.LV_MINER: 3,
        Item.LV_16X_BBUFFER: 3
    }
    summary = summarise(item_dict, book)
    print_summary(summary)

if __name__ == '__main__':
    main()