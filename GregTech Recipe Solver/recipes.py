from enum import Enum
from typing import Dict, List, Optional, Tuple

class Item(Enum):
    NONE = 0
    
    INGOT_COPPER      = 1
    INGOT_TIN         = 2
    INGOT_IRON        = 3
    INGOT_STEEL       = 4
    INGOT_RUBBER      = 5
    INGOT_REDALLOY    = 6
    INGOT_WROUGHTIRON = 7
    INGOT_BRONZE      = 8
    INGOT_ZINC        = 9
    INGOT_BRASS       = 10
    INGOT_GOLD        = 11
    INGOT_SILVER      = 12
    INGOT_LEAD        = 13
    REDSTONE          = 14
    COAGULATED_BLOOD  = 15
    COAL_DUST         = 16
    QUARTZ_SAND       = 17
    FLINT_DUST        = 18
    WOOD_PULP         = 19
    DIAMOND           = 20
    CERTUS_QUARTZ     = 21
    BRICKS            = 22
    LIQUID_REFINED_GLUE = 23
    OBSIDIAN            = 24
    DUST_GYPSUM         = 25
    DUST_BRICK          = 26
    DUST_CLAY           = 27
    DUST_CALCITE        = 28
    DUST_STONE          = 29
    COBBLESTONE         = 30
    WOOD_PLANKS         = 31
    STRING              = 32
    LEATHER             = 33
    COTTON              = 34

    WIRE_COPPER         = 101
    WIRE_TIN            = 102
    WIRE_REDALLOY       = 103
    WIRE_GOLD           = 104
    ROD_IRON            = 105
    ROD_STEEL           = 106
    ROD_REDALLOY        = 107
    ROD_TIN             = 108
    ROD_BRASS           = 109
    PLATE_STEEL         = 110
    PLATE_RUBBER        = 111
    PLATE_WROUGHTIRON   = 112
    PLATE_BRONZE        = 113
    PLATE_TIN           = 114
    PLATE_SILVER        = 115
    PLATE_IRON          = 116
    PLATE_COPPER        = 117
    CASING_STEEL        = 118
    GLASS_DUST          = 119
    WOOD_PLANK          = 120
    RING_RUBBER         = 121
    OBSIDIAN_DUST       = 122
    LIQUID_RED_ALLOY    = 123
    BLOCK_IRON          = 124
    DUST_FIRECLAY       = 125
    BUCKET_CONCRETE     = 126
    FURNACE             = 127
    LIQUID_LEAD         = 128
    WIRE_16X_TIN        = 129
    CHEST               = 130
    WOVEN_COTTON        = 131
    
    SMALL_GEAR_STEEL     = 201
    CABLE_TIN            = 202
    CABLE_REDALLOY       = 203
    ROD_MAGNETIC_IRON    = 204
    GLASS_TUBE           = 205
    FINE_COPPER_WIRE     = 206
    BOLT_REDALLOY        = 207
    BOLT_TIN             = 208
    BOLT_IRON            = 209
    COATED_CIRCUIT_BOARD = 210
    LV_MACHINE_CASING    = 211
    WIRE_2X_TIN          = 212
    WIRE_2X_COPPER       = 213
    WIRE_4X_COPPER       = 214
    PIPE_BRONZE          = 215
    PIPE_STEEL_SMALL     = 216
    RING_TIN             = 217
    RING_IRON            = 218
    RING_STEEL           = 219
    GLASS                = 220
    GLASS_CLEAR          = 221
    PLATE_3X_SILVER      = 222
    MIXED_METAL_INGOT    = 223
    BRICKED_WROUGHT_HULL = 224
    SENSE_STEEL          = 225
    INGOT_FIRECLAY       = 226
    IRON_FURNACE         = 227
    FOIL_COPPER          = 228
    BOUND_LEATHER        = 229

    LV_ELECTRIC_MOTOR = 301
    VACUUM_TUBE       = 302
    RESISTOR          = 303
    CIRCUIT_BOARD     = 304
    LV_MACHINE_HULL   = 305
    SCREW_TIN         = 306
    SCREW_IRON        = 307
    ADVANCED_ALLOY    = 308
    OBSIDIAN_GLASS    = 309
    FIREBRICKS        = 310
    BACKPACK          = 311
    
    LV_ELECTRIC_PISTON  = 401
    LV_ELECTRIC_CIRCUIT = 402
    LV_POLARIZER        = 403
    LV_CONVEYOR         = 404
    ROTOR_TIN           = 405
    REINFORCED_GLASS    = 406
    TANK                = 407
    ANVIL               = 408
    CONTROLLER_BBF      = 409
    LV_16X_BBUFFER       = 410
    
    LV_BENDING_MACHINE = 501
    LV_LATHE           = 502
    LV_CENTRIFUGE      = 503
    LV_STEAM_TURBINE   = 504
    LV_SENSOR          = 505
    LV_ROBOT_ARM       = 506
    LV_EXTRUDER        = 507
    LV_ELECTRIC_PUMP   = 508
    LV_ELECTROLYZER    = 509
    HP_SOLAR_BOILER    = 510
    LV_EMITTER         = 511
    LV_INPUT_HATCH     = 512
    LV_WIREMILL        = 513
    LV_ALLOY_SMELTER   = 514
    LV_COMPRESSOR      = 515
    LV_FURNACE         = 516
    LV_MACERATOR       = 517
    LV_FORGE_HAMMER    = 518
    
    LV_MINER           = 601
    LV_ASSEMBLER       = 602
    LV_FLUID_EXTRACTOR = 603
    LV_CROP_MANAGER    = 604
    LV_EXTRACTOR       = 605
    LV_CANNING_MACHINE = 606

class Recipe:
    product: Item
    num_produced: int
    components: Dict[Item, int]
    
    def __init__(self, product: Item, num_produced: int, components: Dict[Item, int]):
        self.product = product
        self.num_produced = num_produced
        self.components = components

class RecipeBook:
    recipes: Dict[Item, Recipe]
    
    def __init__(self):
        self.recipes = {}
    
    def add_recipe(self, product: Item, num_produced: int, components: Dict[Item, int]):
        self.recipes[product] =  Recipe(product, num_produced, components)
    
    def get_recipe(self, product: Item) -> Recipe:
        if product in self.recipes:
            return self.recipes[product]
        else:
            raise RuntimeError(f"No recipe for item {product}")

def get_full_book() -> RecipeBook:
    this_book = RecipeBook()
    this_book.add_recipe(Item.WIRE_COPPER, 2, {
        Item.INGOT_COPPER: 1
    })
    this_book.add_recipe(Item.WIRE_TIN, 2, {
        Item.INGOT_TIN: 1
    })
    this_book.add_recipe(Item.WIRE_REDALLOY, 2, {
        Item.INGOT_REDALLOY: 1
    })
    this_book.add_recipe(Item.WIRE_GOLD, 2, {
        Item.INGOT_GOLD: 1
    })
    this_book.add_recipe(Item.ROD_IRON, 1, {
        Item.INGOT_IRON: 1
    })
    this_book.add_recipe(Item.ROD_STEEL, 1, {
        Item.INGOT_STEEL: 1
    })
    this_book.add_recipe(Item.ROD_TIN, 1, {
        Item.INGOT_TIN: 1
    })
    this_book.add_recipe(Item.ROD_REDALLOY, 1, {
        Item.INGOT_REDALLOY: 1
    })
    this_book.add_recipe(Item.ROD_BRASS, 1, {
        Item.INGOT_BRASS: 1
    })
    this_book.add_recipe(Item.BOLT_REDALLOY, 2, {
        Item.ROD_REDALLOY: 1
    })
    this_book.add_recipe(Item.BOLT_TIN, 2, {
        Item.ROD_TIN: 1
    })
    this_book.add_recipe(Item.SCREW_TIN, 1, {
        Item.BOLT_TIN: 2
    })
    this_book.add_recipe(Item.BOLT_IRON, 2, {
        Item.ROD_IRON: 1
    })
    this_book.add_recipe(Item.SCREW_IRON, 1, {
        Item.BOLT_IRON: 2
    })
    this_book.add_recipe(Item.RING_TIN, 1, {
        Item.ROD_TIN: 1
    })
    this_book.add_recipe(Item.RING_IRON, 1, {
        Item.ROD_IRON: 1
    })
    this_book.add_recipe(Item.RING_RUBBER, 4, {
        Item.INGOT_RUBBER: 1
    })
    this_book.add_recipe(Item.PLATE_RUBBER, 1, {
        Item.INGOT_RUBBER: 1
    })
    this_book.add_recipe(Item.PLATE_STEEL, 1, {
        Item.INGOT_STEEL: 1
    })
    this_book.add_recipe(Item.PLATE_BRONZE, 1, {
        Item.INGOT_BRONZE: 1
    })
    this_book.add_recipe(Item.PLATE_WROUGHTIRON, 1, {
        Item.INGOT_WROUGHTIRON: 1
    })
    this_book.add_recipe(Item.PLATE_TIN, 1, {
        Item.INGOT_TIN: 1
    })
    this_book.add_recipe(Item.PLATE_SILVER, 1, {
        Item.INGOT_SILVER: 1
    })
    this_book.add_recipe(Item.PLATE_IRON, 1, {
        Item.INGOT_IRON: 1
    })
    this_book.add_recipe(Item.PLATE_COPPER, 1, {
        Item.INGOT_COPPER: 1
    })
    this_book.add_recipe(Item.PLATE_3X_SILVER, 1, {
        Item.PLATE_SILVER: 3
    })
    this_book.add_recipe(Item.FOIL_COPPER, 4, {
        Item.PLATE_COPPER: 1
    })
    this_book.add_recipe(Item.CASING_STEEL, 3, {
        Item.INGOT_STEEL: 2
    })
    this_book.add_recipe(Item.ROD_MAGNETIC_IRON, 1, {
        Item.ROD_IRON: 1
    })
    this_book.add_recipe(Item.CABLE_TIN, 1, {
        Item.WIRE_TIN: 1,
        Item.PLATE_RUBBER: 1
    })
    this_book.add_recipe(Item.CABLE_REDALLOY, 1, {
        Item.WIRE_REDALLOY: 1,
        Item.PLATE_RUBBER: 1
    })
    this_book.add_recipe(Item.SMALL_GEAR_STEEL, 1, {
        Item.PLATE_STEEL: 1,
        Item.ROD_STEEL: 2
    })
    this_book.add_recipe(Item.LV_ELECTRIC_MOTOR, 1, {
        Item.ROD_MAGNETIC_IRON: 1,
        Item.ROD_IRON: 2,
        Item.WIRE_COPPER: 4,
        Item.CABLE_TIN: 2
    })
    this_book.add_recipe(Item.LV_ELECTRIC_PISTON, 1, {
        Item.PLATE_STEEL: 3,
        Item.ROD_STEEL: 2,
        Item.CABLE_TIN: 2,
        Item.LV_ELECTRIC_MOTOR: 1,
        Item.SMALL_GEAR_STEEL: 1
    })
    this_book.add_recipe(Item.VACUUM_TUBE, 1, {
        Item.ROD_STEEL: 1,
        Item.WIRE_COPPER: 1,
        Item.GLASS_TUBE: 1,
        Item.LIQUID_RED_ALLOY: 18
    })
    this_book.add_recipe(Item.FINE_COPPER_WIRE, 4, {
        Item.WIRE_COPPER: 1
    })
    this_book.add_recipe(Item.GLASS_TUBE, 1, {
        Item.GLASS_DUST: 1
    })
    this_book.add_recipe(Item.GLASS_DUST, 8, {
        Item.QUARTZ_SAND: 8,
        Item.FLINT_DUST: 1
    })
    this_book.add_recipe(Item.WOOD_PLANK, 1, {
        Item.WOOD_PULP: 1
    })
    this_book.add_recipe(Item.COATED_CIRCUIT_BOARD, 1, {
        Item.COAGULATED_BLOOD: 2,
        Item.WOOD_PLANK: 1
    })
    this_book.add_recipe(Item.CIRCUIT_BOARD, 1, {
        Item.WOOD_PLANK: 1,
        Item.FOIL_COPPER: 4,
        Item.LIQUID_REFINED_GLUE: 72
    })
    this_book.add_recipe(Item.RESISTOR, 4, {
        Item.WIRE_COPPER: 4,
        Item.FINE_COPPER_WIRE: 4,
        Item.COAL_DUST: 1,
        Item.LIQUID_REFINED_GLUE: 288
    })
    this_book.add_recipe(Item.LV_ELECTRIC_CIRCUIT, 1, {
        Item.RESISTOR: 2,
        Item.VACUUM_TUBE: 2,
        Item.CIRCUIT_BOARD: 1,
        Item.CABLE_REDALLOY: 3,
        Item.CASING_STEEL: 1
    })
    # this_book.add_recipe(Item.LV_ELECTRIC_CIRCUIT, 1, {
    #     Item.RESISTOR: 2,
    #     Item.VACUUM_TUBE: 2,
    #     Item.CIRCUIT_BOARD: 1,
    #     Item.WIRE_REDALLOY: 2,
    #     Item.LIQUID_LEAD: 288
    # })
    this_book.add_recipe(Item.LV_MACHINE_CASING, 1, {
        Item.PLATE_STEEL: 8
    })
    this_book.add_recipe(Item.LV_MACHINE_HULL, 1, {
        Item.PLATE_STEEL: 1,
        Item.PLATE_WROUGHTIRON: 2,
        Item.CABLE_TIN: 2,
        Item.LV_MACHINE_CASING: 1
    })
    this_book.add_recipe(Item.LV_MACHINE_HULL, 1, {
        Item.PLATE_STEEL: 1,
        Item.PLATE_WROUGHTIRON: 2,
        Item.CABLE_TIN: 2,
        Item.LV_MACHINE_CASING: 1
    })
    this_book.add_recipe(Item.LV_BENDING_MACHINE, 1, {
        Item.LV_ELECTRIC_PISTON: 2,
        Item.LV_ELECTRIC_MOTOR: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.CABLE_TIN: 1
    })
    this_book.add_recipe(Item.LV_LATHE, 1, {
        Item.LV_ELECTRIC_PISTON: 1,
        Item.LV_ELECTRIC_MOTOR: 1,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.CABLE_TIN: 3,
        Item.DIAMOND: 1
    })
    this_book.add_recipe(Item.WIRE_2X_TIN, 1, {
        Item.WIRE_TIN: 2
    })
    this_book.add_recipe(Item.WIRE_2X_COPPER, 1, {
        Item.WIRE_COPPER: 2
    })
    this_book.add_recipe(Item.LV_POLARIZER, 1, {
        Item.WIRE_2X_TIN: 4,
        Item.CABLE_TIN: 2,
        Item.ROD_IRON: 2,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.LV_CENTRIFUGE, 1, {
        Item.LV_ELECTRIC_MOTOR: 2,
        Item.LV_ELECTRIC_CIRCUIT: 4,
        Item.LV_MACHINE_HULL: 1,
        Item.CABLE_TIN: 2,
    })
    this_book.add_recipe(Item.LV_CONVEYOR, 1, {
        Item.PLATE_RUBBER: 6,
        Item.LV_ELECTRIC_MOTOR: 2,
        Item.CABLE_TIN: 1,
    })
    this_book.add_recipe(Item.PIPE_BRONZE, 2, {
        Item.PLATE_BRONZE: 6
    })
    this_book.add_recipe(Item.ROTOR_TIN, 1, {
        Item.PLATE_TIN: 4,
        Item.RING_TIN: 1,
        Item.SCREW_TIN: 1
    })
    this_book.add_recipe(Item.LV_STEAM_TURBINE, 1, {
        Item.LV_ELECTRIC_MOTOR: 2,
        Item.LV_ELECTRIC_CIRCUIT: 1,
        Item.LV_MACHINE_HULL: 1,
        Item.ROTOR_TIN: 2,
        Item.CABLE_TIN: 1,
        Item.PIPE_BRONZE: 2
    })
    this_book.add_recipe(Item.LV_SENSOR, 1, {
        Item.LV_ELECTRIC_CIRCUIT: 1,
        Item.PLATE_STEEL: 4,
        Item.ROD_BRASS: 1,
        Item.CERTUS_QUARTZ: 1
    })
    this_book.add_recipe(Item.LV_MINER, 1, {
        Item.LV_ELECTRIC_MOTOR: 3,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.CABLE_TIN: 2,
        Item.LV_SENSOR: 1
    })
    this_book.add_recipe(Item.LV_ROBOT_ARM, 1, {
        Item.LV_ELECTRIC_MOTOR: 2,
        Item.LV_ELECTRIC_CIRCUIT: 1,
        Item.LV_ELECTRIC_PISTON: 1,
        Item.ROD_STEEL: 2,
        Item.CABLE_TIN: 3
    })
    this_book.add_recipe(Item.LV_ASSEMBLER, 1, {
        Item.LV_ROBOT_ARM: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_CONVEYOR: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.CABLE_TIN: 2
    })
    this_book.add_recipe(Item.WIRE_4X_COPPER, 1, {
        Item.WIRE_COPPER: 4
    })
    this_book.add_recipe(Item.LV_EXTRUDER, 1, {
        Item.LV_ELECTRIC_PISTON: 1,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.PIPE_BRONZE: 1,
        Item.LV_MACHINE_HULL: 1,
        Item.WIRE_4X_COPPER: 4
    })
    this_book.add_recipe(Item.LV_ELECTRIC_PUMP, 1, {
        Item.LV_ELECTRIC_MOTOR: 1,
        Item.ROTOR_TIN: 1,
        Item.PIPE_BRONZE: 1,
        Item.CABLE_TIN: 1,
        Item.SCREW_TIN: 1,
        Item.RING_RUBBER: 2
    })
    this_book.add_recipe(Item.LV_FLUID_EXTRACTOR, 1, {
        Item.LV_ELECTRIC_PISTON: 1,
        Item.LV_ELECTRIC_PUMP: 1,
        Item.GLASS: 2,
        Item.CABLE_TIN: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.GLASS, 1, {
        Item.GLASS_DUST: 1
    })
    this_book.add_recipe(Item.LV_ELECTROLYZER, 1, {
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.WIRE_GOLD: 4,
        Item.GLASS: 1,
        Item.CABLE_TIN: 1,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.HP_SOLAR_BOILER, 1, {
        Item.REINFORCED_GLASS: 3,
        Item.PLATE_3X_SILVER: 3,
        Item.PIPE_STEEL_SMALL: 2,
        Item.BRICKED_WROUGHT_HULL: 1
    })
    this_book.add_recipe(Item.REINFORCED_GLASS, 4, {
        Item.ADVANCED_ALLOY: 1,
        Item.GLASS_DUST: 3
    })
    this_book.add_recipe(Item.ADVANCED_ALLOY, 1, {
        Item.PLATE_IRON: 1,
        Item.PLATE_BRONZE: 1,
        Item.PLATE_TIN: 1,
    })
    this_book.add_recipe(Item.PIPE_STEEL_SMALL, 6, {
        Item.PLATE_STEEL: 6
    })
    this_book.add_recipe(Item.BRICKED_WROUGHT_HULL, 1, {
        Item.PLATE_WROUGHTIRON: 5,
        Item.BRICKS: 3
    })
    this_book.add_recipe(Item.LV_EMITTER, 1, {
        Item.ROD_BRASS: 4,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.CABLE_TIN: 2,
        Item.CERTUS_QUARTZ: 1
    })
    this_book.add_recipe(Item.SENSE_STEEL, 1, {
        Item.PLATE_STEEL: 2,
        Item.INGOT_STEEL: 1
    })
    this_book.add_recipe(Item.LV_INPUT_HATCH, 1, {
        Item.TANK: 1,
        Item.LV_MACHINE_HULL: 1,
        Item.LIQUID_REFINED_GLUE: 720
    })
    this_book.add_recipe(Item.TANK, 1, {
        Item.OBSIDIAN_GLASS: 1,
        Item.RING_IRON: 2
    })
    this_book.add_recipe(Item.OBSIDIAN_GLASS, 1, {
        Item.GLASS_CLEAR: 1,
        Item.OBSIDIAN_DUST: 4
    })
    this_book.add_recipe(Item.GLASS_CLEAR, 1, {
        Item.GLASS_DUST: 1
    })
    this_book.add_recipe(Item.OBSIDIAN_DUST, 1, {
        Item.OBSIDIAN: 1
    })
    this_book.add_recipe(Item.LV_CROP_MANAGER, 1, {
        Item.LV_ROBOT_ARM: 2,
        Item.SENSE_STEEL: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_SENSOR: 1,
        Item.LV_MACHINE_HULL: 1,
        Item.LV_INPUT_HATCH: 1,
    })
    this_book.add_recipe(Item.LIQUID_RED_ALLOY, 144, {
        Item.INGOT_REDALLOY: 1
    })
    this_book.add_recipe(Item.LIQUID_LEAD, 144, {
        Item.INGOT_LEAD: 1
    })
    this_book.add_recipe(Item.LV_WIREMILL, 1, {
        Item.LV_ELECTRIC_MOTOR: 4,
        Item.CABLE_TIN: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.LV_ALLOY_SMELTER, 1, {
        Item.WIRE_4X_COPPER: 4,
        Item.CABLE_TIN: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.LV_COMPRESSOR, 1, {
        Item.LV_ELECTRIC_PISTON: 2,
        Item.CABLE_TIN: 4,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.LV_FURNACE, 1, {
        Item.WIRE_2X_COPPER: 4,
        Item.CABLE_TIN: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1
    })
    this_book.add_recipe(Item.LV_EXTRACTOR, 1, {
        Item.LV_ELECTRIC_PISTON: 1,
        Item.LV_ELECTRIC_PUMP: 1,
        Item.CABLE_TIN: 2,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.GLASS: 2
    })
    this_book.add_recipe(Item.LV_MACERATOR, 1, {
        Item.LV_ELECTRIC_PISTON: 1,
        Item.LV_ELECTRIC_MOTOR: 1,
        Item.CABLE_TIN: 3,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.DIAMOND: 1
    })
    this_book.add_recipe(Item.BLOCK_IRON, 1, {
        Item.INGOT_IRON: 9
    })
    this_book.add_recipe(Item.ANVIL, 1, {
        Item.BLOCK_IRON: 5,
        Item.PLATE_IRON: 2,
        Item.SCREW_IRON: 2
    })
    this_book.add_recipe(Item.LV_FORGE_HAMMER, 1, {
        Item.LV_ELECTRIC_PISTON: 1,
        Item.CABLE_TIN: 4,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.LV_MACHINE_HULL: 1,
        Item.ANVIL: 1
    })
    this_book.add_recipe(Item.FIREBRICKS, 1, {
        Item.INGOT_FIRECLAY: 6,
        Item.DUST_GYPSUM: 2,
        Item.BUCKET_CONCRETE: 1
    })
    this_book.add_recipe(Item.INGOT_FIRECLAY, 1, {
        Item.DUST_FIRECLAY: 1
    })
    this_book.add_recipe(Item.DUST_FIRECLAY, 2, {
        Item.DUST_BRICK: 1,
        Item.DUST_CLAY: 1
    })
    this_book.add_recipe(Item.BUCKET_CONCRETE, 1, {
        Item.DUST_STONE: 1,
        Item.DUST_CLAY: 1,
        Item.QUARTZ_SAND: 1,
        Item.DUST_CALCITE: 2
    })
    this_book.add_recipe(Item.FURNACE, 1, {
        Item.COBBLESTONE: 8
    })
    this_book.add_recipe(Item.IRON_FURNACE, 1, {
        Item.FURNACE: 1,
        Item.PLATE_IRON: 5
    })
    this_book.add_recipe(Item.CONTROLLER_BBF, 1, {
        Item.IRON_FURNACE: 4,
        Item.FIREBRICKS: 4
    })
    this_book.add_recipe(Item.LV_CANNING_MACHINE, 1, {
        Item.LV_MACHINE_HULL: 1,
        Item.LV_ELECTRIC_CIRCUIT: 2,
        Item.CABLE_TIN: 2,
        Item.GLASS: 3,
        Item.LV_ELECTRIC_PUMP: 1
    })
    this_book.add_recipe(Item.WIRE_16X_TIN, 1, {
        Item.INGOT_TIN: 8
    })
    this_book.add_recipe(Item.CHEST, 1, {
        Item.WOOD_PLANKS: 8
    })
    this_book.add_recipe(Item.LV_16X_BBUFFER, 1, {
        Item.WIRE_16X_TIN: 4,
        Item.LV_MACHINE_HULL: 1,
        Item.CHEST: 1
    })
    this_book.add_recipe(Item.WOVEN_COTTON, 1, {
        Item.COTTON: 4,
        Item.STRING: 5
    })
    this_book.add_recipe(Item.BOUND_LEATHER, 1, {
        Item.WOVEN_COTTON: 1,
        Item.STRING: 4,
        Item.LEATHER: 4
    })
    this_book.add_recipe(Item.BACKPACK, 1, {
        Item.WOVEN_COTTON: 3,
        Item.BOUND_LEATHER: 5,
        Item.RING_STEEL: 1
    })
    this_book.add_recipe(Item.RING_STEEL, 1, {
        Item.ROD_STEEL: 1
    })
    
    return this_book