# GregTech Recipe Solver

An iterative solver for collating a list of steps for crafting complex recipes in a modded Minecraft instance.

`recipes.py` provides the `Item` definition, and a set of `Recipe`s in a `RecipeBook` that represent the `Item`s required to produce others.
`crafting_solver.py` provides a `summarise()` function that takes a set of `Item`s to craft, and generates a breakdown of the stages of resources required to produce them.

To-do items:
- Change solver to automatically resolve `Item` "tiering" (as opposed to requiring manual definition of tier).
- Switch `Item` and `Recipe` definition to be via JSON (or some other data file) rather than `Enum` definition.
- Provide interactive functionality for `Item` and `Recipe` definition.
- Provide functionality for querying solver directly (either via command line, interactive prompt, or GUI).
