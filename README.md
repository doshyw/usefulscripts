# Useful Scripts

The following provides a brief outline of the scripts in this repository, and their purpose/function:

|          Name          | Outline |
|:----------------------|:-------|
|  Facebook Data Parsing | A simple script that is able to parse the JSON data dump that can be requested from Facebook at https://www.facebook.com/dyi. |
| GregTech Recipe Solver | An iterative solver for collating a list of steps for crafting complex recipes in a modded Minecraft instance. |
